package jm.edu.utech.ap.welcomelib;

public interface IWelcomeService {
	
	String DEFAULT_WELCOME_PHASE = "Thank you for coming ";

	/**
	 * Prepares and returns a <h1> welcome <h1> message
	 * @param name Name of person to welcome*/
	String getWelcomeMessage(String name);
}
